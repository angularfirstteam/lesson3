import { Component } from '@angular/core';

export class Watch {
  id: number;
  model: string;
  color: string;
  template_type: string;
  system_type: string;
}

const WATCHES: Watch[] = [
  { id: 1, model: 'AAA',  color: 'blue', template_type: 'mwtal', system_type: 'digital'},
  { id: 2, model: 'BBB',  color: 'green', template_type: 'metal', system_type: 'mechanical'},
  { id: 3, model: 'CCC',  color: 'brown', template_type: 'plastic', system_type: 'digital'},
  { id: 4, model: 'DDD',  color: 'black', template_type: 'plastic', system_type: 'digital'},
  { id: 5, model: 'EEE',  color: 'white', template_type: 'metal', system_type: 'mechanical'},
  { id: 6, model: 'FFF',  color: 'gold', template_type: 'plastic', system_type: 'digital'},
  { id: 7, model: 'GGG',  color: 'gold', template_type: 'metal', system_type: 'mechanical'},
  { id: 8, model: 'HHH',  color: 'gray', template_type: 'ebony', system_type: 'digital'},
  { id: 9, model: 'III',  color: 'none', template_type: 'ebony', system_type: 'mechanical'},
 
];


@Component({
  selector: 'app-watch',
  template: `
    <h2>Watches</h2>
    <ul>
      <li *ngFor="let watch of watches">
        <span>{{ watch.id }}</span> 
        <span>{{ watch.model }}</span>
        <span>{{ watch.color }} </span>
        <span>{{ watch.template_type }}</span>
        <span>{{ watch.system_type }}</span>
      </li>
    </ul>
    <h2>{{selectedHero.name}} details!</h2>
  `,
  styles: [
    ` ul {
        list-style-type: none;
      }
      ul li {
        border: 1px solid gray;
        border-radius: 5px;
        padding: 5px;
        width: 310px;
        background-color: lightblue;
        font-size: 18px;
        margin-bottom: 3px;
      }

      ul li span:not(:first-child) {
        display: inline-block;
        width: 65px;
      }

      ul li span:nth-child(1) {
        color: yellow;
      }

    `
  ]
})


export class WatchComponent {
  watches = WATCHES;
}